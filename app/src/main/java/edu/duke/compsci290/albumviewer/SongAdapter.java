package edu.duke.compsci290.albumviewer;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

/**
 * Created by kayladerman on 1/29/18.
 */

public class SongAdapter extends RecyclerView.Adapter<SongAdapter.ViewHolder>{


    Context mContext;
    String[] mSongs;

    public SongAdapter(final Context context, String[] songs) {
        this.mContext = context;
        this.mSongs = songs;
    }

    @Override
    public int getItemCount() {
        return mSongs.length;
    }

    @Override
    public SongAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = mInflater.inflate(R.layout.song_holder, parent, false);
        final SongAdapter.ViewHolder songHolder = new SongAdapter.ViewHolder(row);
        return songHolder;

    }

    @Override
    public void onBindViewHolder(SongAdapter.ViewHolder holder, int position) {
        holder.mSongName.setText(mSongs[position]);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView mSongName;

        public ViewHolder(View itemView) {
            super(itemView);
            this.mSongName = itemView.findViewById(R.id.song_name_text_view);
        }
    }
}