package edu.duke.compsci290.albumviewer;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

public class AlbumActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);

        Intent receivedIntent = this.getIntent();
        String albumName = receivedIntent.getStringExtra("album_name_key");
        String albumN = albumName.replaceAll(" ", "");

        int songID = this.getResources().getIdentifier(albumN, "array", this.getPackageName());

        String[] songs = this.getResources().getStringArray(songID);

        String albumNameL = albumName.toLowerCase().replaceAll("\\W+", "");
        int drawableId = this.getResources().getIdentifier(albumNameL, "drawable", this.getPackageName());
        Drawable albumArtwork = this.getDrawable(drawableId);
        ImageView albumArt = findViewById(R.id.album_artwork_on_click);
        albumArt.setImageDrawable(albumArtwork);
        TextView albumTitle = findViewById(R.id.album_name_on_click);
        albumTitle.setText(albumName);

        String[] artists = this.getResources().getStringArray(R.array.artist_names);
        String[] albums = this.getResources().getStringArray(R.array.album_names);


        int count = 0;
        for (String a : albums) {
            if (a.equals(albumName)) {
                break;
            }
            count ++;
        }



        String artist = artists[count];

        TextView albumArtist = findViewById(R.id.artist_name_on_click);
        albumArtist.setText("By " + artist);

        RecyclerView rv = findViewById(R.id.activity_album_recycler_view);
        rv.setAdapter(new SongAdapter(this, songs));
        rv.setLayoutManager(new LinearLayoutManager(this));
    }
}
