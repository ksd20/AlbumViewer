package edu.duke.compsci290.albumviewer;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.nfc.Tag;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by kayladerman on 1/25/18.
 */

public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.ViewHolder>{

    public static final String TAG = "tag";
    Context mContext;
    String[] mAlbums;
    String[] mArtists;

    public AlbumAdapter(final Context context, String[] albums, String[] artists) {
        this.mContext = context;
        this.mAlbums = albums;
        this.mArtists = artists;
    }

    @Override
    public int getItemCount() {
        return mAlbums.length;
    }

    private void openAlbum(String albumName) {
        Log.d(TAG, "Opening album " + albumName);
        Intent intent = new Intent(mContext, AlbumActivity.class);
        intent.putExtra("album_name_key", albumName);
        mContext.startActivity(intent);
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = mInflater.inflate(R.layout.album_holder, parent, false);
        final ViewHolder albumHolder = new ViewHolder(row);
        albumHolder.mLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openAlbum(mAlbums[albumHolder.getAdapterPosition()]);
            }
        });
        return albumHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //Drawable albumArtwork = mContext.getDrawable(android.R.drawable.ic_dialog_info);
        String albumName = mAlbums[position].toLowerCase().replaceAll("\\W+", "");
        int drawableId = mContext.getResources().getIdentifier(albumName, "drawable", mContext.getPackageName());
        Drawable albumArtwork = mContext.getDrawable(drawableId);
        holder.mImageView.setImageDrawable(albumArtwork);
        holder.mAlbumName.setText(mAlbums[position]);
        // set artist name here
        holder.mArtist.setText("By " + mArtists[position]);

    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        LinearLayout mLinearLayout;
        ImageView mImageView;
        TextView mAlbumName;
        TextView mArtist;

        public ViewHolder(View itemView) {
            super(itemView);
            this.mLinearLayout = itemView.findViewById(R.id.album_holder_linear_layout);
            this.mImageView = itemView.findViewById(R.id.album_artwork_image_view);
            this.mAlbumName = itemView.findViewById(R.id.album_name_text_view);
            this.mArtist = itemView.findViewById(R.id.artist_name_text_view);
        }
    }
}
